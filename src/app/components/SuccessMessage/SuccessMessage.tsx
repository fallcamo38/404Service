// SuccessMessage.js
import React from "react";
import Typography from "@mui/material/Typography";

function SuccessMessage() {
  return (
    <div>
      <Typography>Форма успешно отправлена!</Typography>
    </div>
  );
}

export default SuccessMessage;
